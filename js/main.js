(function () {
    // #region
    const body = document.body;
    const game = document.querySelector('#game');
    const checkerboard = document.querySelector('#checkerboard');
    const dice = document.querySelector('.dice');
    const boy = document.querySelector('#boy');
    const girl = document.querySelector('#girl');
    const home = document.querySelector('#home');
    const rule = document.querySelector('.rule');
    const showTask = document.querySelector('.showTask');
    const receive = showTask.querySelector('.card .receive');
    const taskContent = showTask.querySelector('.card .content');
    const headline = document.querySelector('.headline');
    const back = headline.querySelector('.back');
    const headTitle = headline.querySelector('.title');
    const customOperate = document.querySelector('.customOperate');
    const affirmCustom = customOperate.querySelector('.affirmCustom');
    const customContent = customOperate.querySelector('.customContent');
    const author = document.querySelector('.author');
    const proceeds = document.querySelector('.proceeds');
    const multiOperate = document.querySelector('.multiOperate');
    const multiNum = document.querySelector('.multiNum');
    const affirmMulti = document.querySelector('.affirmMulti');
    const showPosture = document.querySelector('.showPosture');
    const postureName = showPosture.querySelector('.postureName');
    const postureImg = showPosture.querySelector('.postureImg');
    const howPosture = showPosture.querySelector('.howPosture');
    const whyPosture = showPosture.querySelector('.whyPosture');
    const hotPosture = showPosture.querySelector('.hotPosture');
    const affirmPosture = showPosture.querySelector('.affirmPosture');
    const throb = document.querySelector('.throb');
    const showEros = document.querySelector('.showEros');
    const erosContent = showEros.querySelector('.erosContent');
    const affirmEros = showEros.querySelector('.affirmEros');
    const refresh = document.querySelector('.refresh');
    const bulletin = document.querySelector('.bulletin')
    const affirmBulletin = bulletin.querySelector('.affirmBulletin');
    const feedback = document.querySelector('.feedback');
    const sendFeedback = document.querySelector('.sendFeedback');
    const exclusive = document.querySelector('.exclusive');
    const copyText = document.querySelector('.copyText');
    const collect = document.querySelector('.collect');

    const WinW = window.innerWidth;
    const WinH = window.innerHeight;
    const boxSize = WinW / 8;
    const space = 3;
    const checkerboardW = boxSize * 7 + space * 6;
    const boxSpace = boxSize + space;
    const IP = 'http://123.57.90.85:9579';
    // const IP = 'http://127.0.0.1:2020';

    let currentRole;
    let boyIndex;
    let girlIndex;
    let currentTaskType;
    let currentTaskList;
    let roleMoveTimer;
    let isMulti;
    let multiRole;
    let multiCurrentRole;
    let userId = localStorage.getItem('aeroplane_chess_userId');

    let moving = false;

    let gameVersion;

    // #endregion 

    // 混淆代码
    // #region 
    var _src = 'sr',
        _eq = '=',
        _resource = 'reso',
        _img = 'im',
        _author = 'aut',
        _jpg = 'jp',
        _bracketsL = '[',
        _bracketsR = ']',
        _colon = '"',
        _slash = '/',
        _dot = '.',
        _pointer = '*',
        _q = 'q',
        _w = 'w',
        _l = 'l',
        _b = 'b',
        _p = 'p',
        _e = 'e',
        _r = 'r',
        _t = 't',
        _y = 'y',
        _u = 'u',
        _i = 'i',
        _f = 'f',
        _o = 'o',
        _a = 'a',
        _h = 'h',
        _g = 'g',
        _d = 'd',
        _z = 'z',
        _j = 'j',
        _k = 'k',
        _x = 'x',
        _c = 'c',
        _v = 'v',
        _n = 'n',
        _m = 'm',
        _s = 's';

    var _zero = 0,
        _one = 1,
        _two = 2,
        _three = 3,
        _four = 4,
        _five = 5,
        _six = 6,
        _seven = 7,
        _eight = 8,
        _nine = 9;

    // 游戏版本
    gameVersion = _v + _two + _dot + _two;

    var count = _zero + _one + _two + _three + _four + _five + _six + _seven + _eight + _nine;

    _jpg += _g;
    _img += _g;
    _src += _c;
    _resource += 'u' + 'rc' + _e;
    _author += 'ho' + 'r';

    var _authorImg = _bracketsL + _src + _eq + _colon;
    _authorImg += _resource + _slash + _img + _slash;
    _authorImg += _author + _dot + _jpg + _colon + _bracketsR;

    var _qwlb = '';
    _qwlb += _bracketsL;
    _qwlb += _q;
    _qwlb += _w;
    _qwlb += _l;
    _qwlb += _b;
    _qwlb += _eq + _colon + gameVersion + _colon;
    _qwlb += _bracketsR;

    var allBodyEl = document['b' + _o + 'dy']['qu' + _e + 'ryS' + _e + 'l' + _e + 'ct' + _o + 'rA' + _l + 'l'](_pointer);

    allBodyEl[_f + 'o' + _r + 'E' + _a + _c + _h](el => el[_s + _e + _t + 'A' + _t + 't' + 'r' + _i + 'b' + _u + _t + 'e'](_q + _w + _l + _b, gameVersion));

    // #endregion 

    // 地图
    const map = [
        // top, left
        [0, 0],
        [0, boxSpace],
        [0, boxSpace * 2],
        [0, boxSpace * 3],
        [0, boxSpace * 4],
        [0, boxSpace * 5],
        [0, boxSpace * 6],

        [boxSpace, boxSpace * 6],
        [boxSpace * 2, boxSpace * 6],
        [boxSpace * 3, boxSpace * 6],
        [boxSpace * 4, boxSpace * 6],
        [boxSpace * 5, boxSpace * 6],
        [boxSpace * 6, boxSpace * 6],
        [boxSpace * 7, boxSpace * 6],
        [boxSpace * 8, boxSpace * 6],
        [boxSpace * 9, boxSpace * 6],
        [boxSpace * 10, boxSpace * 6],
        [boxSpace * 11, boxSpace * 6],
        [boxSpace * 12, boxSpace * 6],

        [boxSpace * 12, boxSpace * 5],
        [boxSpace * 12, boxSpace * 4],
        [boxSpace * 12, boxSpace * 3],
        [boxSpace * 12, boxSpace * 2],
        [boxSpace * 12, boxSpace],
        [boxSpace * 12, 0],

        [boxSpace * 11, 0],
        [boxSpace * 10, 0],
        [boxSpace * 9, 0],
        [boxSpace * 8, 0],
        [boxSpace * 7, 0],
        [boxSpace * 6, 0],
        [boxSpace * 5, 0],
        [boxSpace * 4, 0],
        [boxSpace * 3, 0],
        [boxSpace * 2, 0],

        [boxSpace * 2, boxSpace],
        [boxSpace * 2, boxSpace * 2],
        [boxSpace * 2, boxSpace * 3],
        [boxSpace * 2, boxSpace * 4],

        [boxSpace * 3, boxSpace * 4],
        [boxSpace * 4, boxSpace * 4],

        [boxSpace * 4, boxSpace * 3],
        [boxSpace * 4, boxSpace * 2],

        [boxSpace * 5, boxSpace * 2],
        [boxSpace * 6, boxSpace * 2],

        [boxSpace * 6, boxSpace * 3],
        [boxSpace * 6, boxSpace * 4],
    ]

    // 任务列表
    const taskList = {
        love: [
            '拍张合照',
            '给对方捶背15秒',
            '给对方按摩小腿15秒',
            '牵手一分钟',
            '对方闭上眼睛给你涂口红',
            '为对方按摩',
            '猪八戒背媳妇',
            '摸对方脸10秒',
            '说说初次见面的感受',
            '接吻3秒',
            '拥抱10秒',
            '对视5秒',
            '喂对方喝水',
            '被对方挠痒痒15秒',
            '学三声猫叫',
            '叫对方姐姐或哥哥',
            '喝同一杯水',
            '在对方耳边轻轻吹一口气',
            '女生坐在男生背上，然后做俯卧撑',
            '公主抱坚持10秒',
            '原地抱头跳跃10次'
        ],
        common: [
            '轻咬对方耳垂5下',
            '对方用嘴给你投食',
            '享受对方按摩两分钟',
            '伸出舌头，互相舔对方舌尖20秒',
            '轻轻的在对方耳朵吹起10下',
            '双方对视20秒',
            '被对方吸允乳头10秒',
            '抱起对方，坚持1分钟',
            '叫对方爸爸',
            '隔着裤子用手摩擦对方下体15秒',
            '手在对方内裤外面任意发挥10秒',
            '摸对方胸部10秒',
            '让对方撅起屁股，打10下',
            '什么都不做',
            '抚摸对方大腿根部30秒',
            '互相拥抱30秒，并摸对方的pp',
            '像口交一样，吸允对方手指10秒',
            '舔对方从脖子到耳朵不低于10秒',
            '脱掉最外面的一层衣服',
            '让对方帮你洗澡',
            '自己喝一杯酒',
            '自己娇喘3次',
            '闻对方的臭袜子',
            '给对方跪地磕一个',
            '隔着裤子亲吻1次对方私处',
            '学叫床3次'
        ],
        high: [
            '让对方拔掉一根毛',
            '使劲捏对方乳头2次',
            '指定任意位置，让对方舔10秒',
            '口交20秒',
            '舔对方的脚15秒',
            '用下体蹭对方的脸部10秒',
            '想办法让对方流水或勃起',
            '跟TA做69，持续15秒',
            '帮对方含住下体，用舌头打转15秒',
            '坐到对方脸上，让对方吸允30秒',
            '跪下来舔对方下体15秒',
            '酸奶倒在自己胸前至下体，然后让对方舔干净',
            '在窗户边上性交30秒',
            '随意指定对方完成一项任务',
            '让对方穿上内裤后舔湿',
            '抽插20次，不许射',
            '跟对方乳交30秒',
            'jj插入菊花3次',
            '自己喝两杯酒',
            '用夹子夹对方乳头30秒',
            '用夹子夹对方下体15秒',
            '让对方吐在手上，自己舔干净',
            '吃下对方的精液',
            '嘴里含住冰块，口交15秒',
            '嘴里含住热水，口交15秒',
            '射在女生脸上',
            '拿笔插入对方的菊花10秒',
            '自慰30秒让对方看',
            '弹对方5次蛋蛋或乳头',
            '本次是点数是多少，就喝多少杯',
        ],
    }

    // 任务类型
    const taskType = [
        {
            describe: '恋爱版',
            mark: 'love'
        },
        {
            describe: '普通版',
            mark: 'common'
        },
        {
            describe: '高阶版',
            mark: 'high'
        },
    ]

    // 方块颜色
    const colorList = [
        '#fabb5c',
        '#fee48f',
        '#84d8fa',
        '#6694f4',
        '#fd995d'
    ]

    // 获取零开头的随机整数
    function randomNumZero(num) {
        return Math.floor(Math.random() * (num + 1));
    }

    // 计算角色坐标
    function calcRoleCoord(box) {
        return {
            top: box[0] + boxSize / 2 - boxSize * 0.8 / 2,
            left: box[1] + boxSize / 2 - boxSize * 0.8 / 2
        }
    }

    // 情侣角色移动
    function loversRoleMove() {
        // 获取骰子的点数
        const indexOf = dice.getAttribute('src').indexOf('dice');
        const points = parseInt(dice.getAttribute('src').slice(indexOf + 4, indexOf + 4 + 1));

        // 计算
        let count = 0;
        const index = currentRole ? boyIndex : girlIndex;
        const role = currentRole ? boy : girl;
        const calcNum = index + points > map.length - 1 ? map.length - 1 : index + points;

        role.style.animation = 'throb .3s infinite';

        for (let i = index + 1; i <= calcNum; i++) {
            setTimeout(() => {
                throb.currentTime = 0;
                throb.play();

                const coord = calcRoleCoord(map[i]);

                role.style.top = coord.top + 'px';
                role.style.left = coord.left + 'px';
                role.style.zIndex = 99;
            }, count * 300);

            count++;
        }

        // 更新角色位置&切换当前角色
        if (currentRole) {
            boyIndex += points;
            currentRole = 0;
        } else {
            girlIndex += points;
            currentRole = 1;
        }

        // 判断是否胜利
        setTimeout(() => {
            moving = false;
            role.style.zIndex = 10;
            role.style.animation = 'none';

            // 防止一直出现重复的
            const num = randomNumZero(currentTaskList.length - 1);
            showTaskFn(currentTaskList[num]);
            currentTaskList.splice(num, 1);

            if (currentTaskList.length < 1) {
                currentTaskList = [...taskList[currentTaskType]];
            }

            if (boyIndex >= map.length - 1) {
                gameOver('男生胜利 可要求对方做任何事情🤩');
            } else if (girlIndex >= map.length - 1) {
                gameOver('女生胜利 可要求对方做任何事情🤩');
            }
        }, count * 300);

    }

    // 多人角色移动
    function multiRoleMove() {
        // 获取骰子的点数
        const indexOf = dice.getAttribute('src').indexOf('dice');
        const points = parseInt(dice.getAttribute('src').slice(indexOf + 4, indexOf + 4 + 1));

        // 计算
        let count = 0;
        const role = checkerboard.querySelector(`[index="${multiCurrentRole}"].role`);
        const roleIndex = parseInt(role.getAttribute('step'));
        const calcNum = roleIndex + points > map.length - 1 ? map.length - 1 : roleIndex + points;

        role.style.animation = 'throb .3s infinite';

        for (let i = roleIndex + 1; i <= calcNum; i++) {
            setTimeout(() => {
                throb.currentTime = 0;
                throb.play();

                const coord = calcRoleCoord(map[i]);

                role.style.top = coord.top + 'px';
                role.style.left = coord.left + 'px';
                role.style.zIndex = 99;
            }, count * 300);

            count++;
        }

        role.setAttribute('step', points + parseInt(role.getAttribute('step')));

        if (multiCurrentRole < multiRole - 1) {
            multiCurrentRole++;
        } else {
            multiCurrentRole = 0;
        }

        // 判断是否胜利
        setTimeout(() => {
            moving = false;
            role.style.zIndex = role.getAttribute('zIndex');
            role.style.animation = 'none';

            // 防止一直出现重复的
            const num = randomNumZero(currentTaskList.length - 1);
            showTaskFn(currentTaskList[num]);
            currentTaskList.splice(num, 1);

            if (currentTaskList.length < 1) {
                currentTaskList = [...taskList[currentTaskType]];
            }

            if (parseInt(role.getAttribute('step')) >= map.length - 1) {
                gameOver('胜利! 可指定某人做任何事情🤩');
            }
        }, count * 300);

    }

    // 游戏结束
    function gameOver(str) {
        moving = true;
        showTaskFn(str);
    }

    // 展示任务
    function showTaskFn(task) {
        showTask.style.display = 'block';
        taskContent.innerText = task;
    }

    // 初始化
    function init() {
        // 变量
        currentRole = 1;
        boyIndex = 0;
        girlIndex = 0;
        moving = false;
        isMulti = false;
        currentTaskList = [];
        multiCurrentRole = 0;
        roleMoveTimer && clearTimeout(roleMoveTimer);

        // 角色位置
        const coord = calcRoleCoord(map[0]);
        boy.style.top = coord.top + 'px';
        boy.style.left = coord.left + 'px';
        girl.style.top = coord.top + 'px';
        girl.style.left = coord.left + 'px';

        goHome();
    }

    // 返回主页
    function goHome() {
        headTitle.innerHTML = '情侣飞行棋';
        // headTitle.innerHTML = `情侣飞行棋<p><span white><a href="http://152.136.51.95:2023/index.html">点击这里去 飞行棋plus</a></span>`;

        home.style.display = 'flex';
        checkerboard.style.display = 'none';
        rule.style.display = 'none';
        back.style.display = 'none';

        girl.style.display = 'block';
        boy.style.display = 'block';
        multiNum.value = '';

        checkerboard.querySelectorAll('[index].role').forEach(role => role.remove());
    }

    // 放大图片
    function magnifyImg() {
        const _this = this;
        recordFrequency(_this.className);
        if (_this.style.width == '96%') {
            _this.style.width = '16%';
            _this.style.left = _this.getAttribute('left');
            _this.style.zIndex = 1;
        } else {
            _this.style.width = '96%';
            _this.style.left = '2%';
            _this.style.zIndex = 2;
        }
    }

    // 记录用户点击次数
    function recordFrequency(key) {
        const frequency = JSON.parse(localStorage.getItem('aeroplane_chess_frequency_info')) || {};

        if (frequency[key]) {
            frequency[key]++;
        } else {
            frequency[key] = 1;
        }

        localStorage.setItem('aeroplane_chess_frequency_info', JSON.stringify(frequency));
    }

    // 展示姿势
    function postureInfo() {
        showPosture.style.display = 'flex';
        const random = randomNumZero(postureList.length - 1);
        const posture = postureList[random];

        postureImg.setAttribute('src', `resource/img/${posture.img}.png`);
        postureName.innerHTML = posture.name;
        howPosture.innerHTML = '<span>怎么做：</span>' + posture.how;
        whyPosture.innerHTML = '<span>为什么：</span>' + posture.why;
        hotPosture.innerHTML = '<span>加热：</span>' + posture.hot;

    }

    // 展示eros知识列表
    function erosInfo() {
        showEros.style.display = 'block';
        const random = randomNumZero(erosList.length - 1);
        erosContent.innerText = erosList[random];
    }

    // 后门,点击6次标题则清除限制时间
    function postern() {
        headTitle.addEventListener('touchend', () => {
            let num = parseInt(headTitle.getAttribute('num')) || 1;

            if (num > 5) {
                localStorage.removeItem('aeroplane_chess_posture_date');
                localStorage.removeItem('aeroplane_chess_eros_date');
                headTitle.setAttribute('num', 1);
            } else {
                headTitle.setAttribute('num', ++num);
            }

        });
    }

    // 包装url
    function wrapUrl(uri) {
        const verify = +new Date() - 1640966400000 + randomNumZero(999) + (randomNumZero(89) + 10) * 100000000000;
        return IP + uri + "?verify=" + verify;
    }

    // 复制内容
    function copyContent(content, hintText) {
        // 回车：String.fromCharCode(10)
        copyText.value = content;
        copyText.select();
        document.execCommand('copy');
        copyText.blur();
        hintText && hint(hintText);
    }

    // 提示
    function hint(content, time) {
        const hint = document.createElement('div');
        hint.classList.add('hint');
        hint.innerText = content;
        body.appendChild(hint);
        setTimeout(() => hint.remove(), time || 5000);
    }

    // 进入全屏
    function launchFullscreen() {
        const html = document.documentElement;

        if (html.requestFullscreen) {
            html.requestFullscreen()
        } else if (html.mozRequestFullScreen) {
            html.mozRequestFullScreen()
        } else if (html.msRequestFullscreen) {
            html.msRequestFullscreen()
        } else if (html.webkitRequestFullscreen) {
            html.webkitRequestFullScreen()
        }
        checkerboard.style.top = WinH - boxSpace * 13.5 + 70 + 'px';
        rule.style.top = (WinH - boxSpace * 13.5) + boxSpace * 7.7 + 70 + 'px';
    }

    // 退出全屏
    function exitFullscreen() {
        if (document.exitFullscreen) {
            document.exitFullscreen()
        } else if (document.msExitFullscreen) {
            document.msExitFullscreen()
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen()
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen()
        }
        checkerboard.style.top = WinH - boxSpace * 13.5 + 'px';
        rule.style.top = (WinH - boxSpace * 13.5) + boxSpace * 7.7 + 'px';
    }

    // 入口
    prepare();
    // setTimeout(prepare, randomNumZero(500) + 500);

    // 准备
    function prepare() {
        // 捕获全局错误并记录
        window.addEventListener('error', e => localStorage.setItem('aeroplane_chess_error', e.error.stack + '---' + (localStorage.getItem('aeroplane_chess_error') || '')));

        // 创建地图
        map.forEach((coord, index) => {
            const box = document.createElement('div');
            box.classList.add('box');
            box.style.width = boxSize + 'px';
            box.style.height = boxSize + 'px';
            box.style.top = coord[0] + 'px';
            box.style.left = coord[1] + 'px';
            box.style.lineHeight = boxSize + 'px';
            box.style.backgroundColor = colorList[randomNumZero(colorList.length - 1)];
            box.setAttribute('index', index);
            box.innerText = index + 1;
            checkerboard.appendChild(box);
        });

        // 创建首页的任务类型
        taskType.forEach(task => {
            const div = document.createElement('div');
            div.innerText = task.describe;
            div.setAttribute('mark', task.mark);
            home.appendChild(div);
        });

        // 设置棋盘居中
        checkerboard.style.top = WinH - boxSpace * 13.5 + 'px';
        checkerboard.style.left = (WinW - checkerboardW) / 2 + 'px';
        // 设置规则div高度
        rule.style.top = (WinH - boxSpace * 13.5) + boxSpace * 7.7 + 'px';

        // 设置角色大小
        boy.style.width = boxSize * 0.8 + 'px';
        girl.style.width = boxSize * 0.8 + 'px';

        init();

        // 摇骰子
        dice.addEventListener('touchend', function () {
            if (moving) {
                return;
            }
            recordFrequency('dice');

            moving = true;
            dice.setAttribute('src', `resource/img/dice${randomNumZero(5) + 1}.gif`);

            if (isMulti) {
                roleMoveTimer = setTimeout(multiRoleMove, 1700);
            } else {
                roleMoveTimer = setTimeout(loversRoleMove, 1700);
            }
        });

        // 关闭任务展示
        receive.addEventListener('touchend', () => showTask.style.display = 'none');

        // 关闭姿势信息展示
        affirmPosture.addEventListener('touchend', () => showPosture.style.display = 'none');

        // 关闭eros知识展示
        affirmEros.addEventListener('touchend', () => showEros.style.display = 'none');

        // 关闭公告
        affirmBulletin.addEventListener('touchend', () => bulletin.style.display = 'none');

        // 收藏
        collect.addEventListener('touchend', () => copyContent(location.href,
            '链接已复制到粘贴板' +
            String.fromCharCode(10) +
            '快去保存到备忘录里吧'
        ));


        // 跳转到plus
        exclusive.addEventListener('touchend', () => {

            const aeroplane_chess_plus = 'http://152.136.51.95/aeroplane_chess/';

            const search = 'discount=discount-9.9';

            const url = aeroplane_chess_plus + '?' + search;

            location.href = url;

        });

        // 开始游戏
        home.querySelectorAll('[mark]').forEach(selectType => {
            selectType.addEventListener('touchend', function () {
                // 如果没有同时满足这两个混淆条件，则return
                if (!document.querySelector(_authorImg) || document.querySelectorAll(_qwlb).length < allBodyEl.length) {
                    return;
                }

                // 复制广告
                // const contact_qq = '2397366245';
                // copyContent('闲置低价好物,联系QQ:' + contact_qq + '（羽毛球、乒乓球拍、捷安特自行车、手表、相机、投影仪 等）');

                // 全屏
                if(window.innerWidth < 390) {
                    launchFullscreen()
                }

                // 发送用户点击次数
                const frequencyDate = parseInt(localStorage.getItem('aeroplane_chess_frequency_date')) || 0;
                if (+new Date() - frequencyDate > 1000 * 60 * 5) {
                    const frequencyInfo = JSON.parse(localStorage.getItem('aeroplane_chess_frequency_info')) || {};
                    frequencyInfo['userId'] = userId;
                    frequencyInfo['game'] = 'luo';

                    axios.post(wrapUrl("/frequency/insert"), frequencyInfo);
                    localStorage.setItem('aeroplane_chess_frequency_info', '{}');

                    localStorage.setItem('aeroplane_chess_frequency_date', +new Date());
                }

                // 设置当前任务类型
                currentTaskType = selectType.getAttribute('mark');
                recordFrequency(currentTaskType);

                if (currentTaskType == 'disabled') {
                    return;
                }

                if (currentTaskType == 'custom' || currentTaskType == 'multiCustom') {
                    customOperate.style.display = 'flex';
                    customOperate.setAttribute('type', currentTaskType);

                    let custom;
                    if (currentTaskType == 'custom') {
                        // 获取自定义
                        custom = JSON.parse(localStorage.getItem('aeroplane_chess_custom')) || ['任务1', '任务2'];
                    } else if (currentTaskType == 'multiCustom') {
                        // 获取多人自定义
                        custom = JSON.parse(localStorage.getItem('aeroplane_chess_multi_custom')) || ['任务1', '任务2'];
                    }

                    let customValue = '';
                    custom.forEach(item => customValue += item + '#');
                    customContent.value = customValue.substr(0, customValue.length - 1);

                    setTimeout(() => customContent.onfocus = null, 200);
                }

                if (currentTaskType == 'ktv') {
                    multiOperate.style.display = 'block';
                }

                if (currentTaskType == 'posture') {
                    const date = parseInt(localStorage.getItem('aeroplane_chess_posture_date'));

                    if (date && +new Date() - date < 1000 * 60 * 7) {
                        alert('你点击的太快了，过7分钟再来吧');
                        return;
                    }

                    localStorage.setItem('aeroplane_chess_posture_date', +new Date());

                    postureInfo();
                    return;
                }

                if (currentTaskType == 'eros') {
                    const date = parseInt(localStorage.getItem('aeroplane_chess_eros_date'));

                    if (date && +new Date() - date < 1000 * 60 * 5) {
                        alert('你点击的太快了，过五分钟再来吧');
                        return;
                    }

                    localStorage.setItem('aeroplane_chess_eros_date', +new Date());

                    erosInfo();
                    return;
                }

                currentTaskList = [...taskList[currentTaskType]];

                const indexOf = selectType.innerHTML.indexOf('<p');
                if (indexOf > 0) {
                    headTitle.innerText = selectType.innerHTML.substring(0, indexOf);
                } else {
                    headTitle.innerText = selectType.innerText;
                }
                home.style.display = 'none';
                checkerboard.style.display = 'block';
                rule.style.display = 'block';
                back.style.display = 'block';
            });
        });

        // 不知道为什么，文本域显示时总会自动获取焦点
        customContent.onfocus = function () {
            customContent.blur();
        }

        back.addEventListener('touchend', init);

        // 开始自定义
        affirmCustom.addEventListener('touchend', () => {
            // 清除回车与空格
            const content = customContent.value.replace(/[\r\n]/g, '').trim();
            if (!content) {
                return alert('自定义内容不能为空');
            }

            const arr = content.split('#');

            if (customOperate.getAttribute('type') == 'custom') {
                localStorage.setItem('aeroplane_chess_custom', JSON.stringify(arr));
                taskList.custom = arr;
            } else if (customOperate.getAttribute('type') == 'multiCustom') {
                localStorage.setItem('aeroplane_chess_multi_custom', JSON.stringify(arr));
                taskList.multiCustom = arr;
                multiOperate.style.display = 'block';
            }

            currentTaskList = [...taskList[currentTaskType]];

            customOperate.style.display = 'none';
            customContent.onfocus = function () {
                customContent.blur();
            }
        });

        // 开始多人游戏
        affirmMulti.addEventListener('touchend', () => {
            if (multiNum.value < 2) {
                return alert('人数太少了，无法继续');
            } else if (multiNum.value > 20) {
                return alert('人数太多了哟');
            }

            isMulti = true;
            multiOperate.style.display = 'none';
            girl.style.display = 'none';
            boy.style.display = 'none';
            multiRole = multiNum.value;

            for (let i = 0; i < multiRole; i++) {
                const role = document.createElement('div');
                role.classList.add('role');
                role.setAttribute('index', i);
                role.setAttribute('step', 0);
                role.setAttribute('zIndex', multiRole - i + 10);
                role.style.width = boxSize * 0.8 + 'px';
                role.style.height = boxSize * 0.8 + 'px';
                role.style.lineHeight = boxSize * 0.8 - 3 + 'px';
                role.innerText = 'P' + (i + 1);
                role.style.zIndex = multiRole - i + 10;
                const coord = calcRoleCoord(map[0]);
                role.style.top = coord.top + 'px';
                role.style.left = coord.left + 'px';
                checkerboard.appendChild(role);
            }
        });

        // 展示作者
        author.addEventListener('touchend', magnifyImg);

        // 展示收款码
        proceeds.addEventListener('touchend', magnifyImg);

        // 刷新网页
        refresh.addEventListener('touchend', () => {
            const date = parseInt(localStorage.getItem('aeroplane_chess_refresh_date'));
            if (date && +new Date() - date < 1000 * 60 * 10) {
                return;
            }
            localStorage.setItem('aeroplane_chess_refresh_date', +new Date());

            location.reload();
        });

        // 后门
        postern();

        // 如果没有用户ID，则设置一个
        if (!userId) {
            const id = +new Date();
            userId = id;
            localStorage.setItem('aeroplane_chess_userId', id);
        }

        // 发送反馈
        sendFeedback.addEventListener('touchend', () => {
            const content = feedback.value.trim();
            if (!content) {
                return alert('内容不能为空');
            } else if (content.length > 100) {
                return alert('内容太长了噢');
            }

            const date = new Date();

            axios.post(wrapUrl("/feedback/insert"), {
                userId: userId,
                content: content,
                date: date.toLocaleDateString() + ' ' + date.toLocaleTimeString()
            }).then(res => {
                if (res.data.code == 200) {
                    feedback.setAttribute('placeholder', '已发送');
                } else {
                    feedback.setAttribute('placeholder', '请不要包含特殊字符');
                }
                feedback.value = '';
            });
        });

        recordFrequency('visitCount');

        document.querySelector('title').innerText = '情侣飞行棋';
        headTitle.innerHTML = `情侣飞行棋`;

        headTitle.style.fontSize = '22px';
        headTitle.style.fontWeight = '800';

        document.querySelector('.loading').style.display = 'none';
    }

    // prepare();


    // JS加密
    // https://www.json.cn/json/jshx.html
    // https://www.sojson.com/jsobfuscator.html
    // https://www.jsjiami.com/

})();