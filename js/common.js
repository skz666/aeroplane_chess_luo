// 添加js脚本
function append_js(name) {
    const body = document.body;
    const script = document.createElement('script');
    script.setAttribute('src', `js/${name}.js?temp=${+new Date()}`);
    body.appendChild(script);
}

// 添加css样式
function append_css(name) {
    const head = document.head;
    const link = document.createElement('link');
    link.setAttribute('rel', 'stylesheet');
    link.setAttribute('href', `css/${name}.css?temp=${+new Date()}`);
    head.appendChild(link);
}
